
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-05-2015 a las 22:08:03
-- Versión del servidor: 5.1.66
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `u180422476_bolet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(65) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `apellido` varchar(65) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `dni` int(8) NOT NULL,
  `id_curso` int(2) NOT NULL,
  `trim1` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `trim2` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `trim3` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `prom` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `nombre`, `apellido`, `dni`, `id_curso`, `trim1`, `trim2`, `trim3`, `prom`) VALUES
(1, 'Albarracin', 'Sebastian', 41199269, 1, '6!7!7!8!9!7!6!8!7!9!7!6!!!!!', '6!7!7!9!8!6!6!8!5!5!5!5!!!!!', '5!3!7!8!9!8!9!7!9!6!9!4!!!!!', ''),
(2, 'Maximiliano', 'Megna', 40652951, 1, '5!5!7!8!7!6!6!4!4!4!5!4!!!!!', '5!6!7!9!8!8!4!7!4!4!4!5!!!!!', '4!1!7!9!7!7!4!10!5!3!6!3!!!!!', ''),
(3, 'Camila', 'Diotisalvi', 41077589, 1, '9!7!9!8!9!8!6!7!8!7!7!8!!!!!', '6!8!8!9!10!7!6!9!6!7!5!8!!!!!', '7!8!8!9!10!9!9!8!8!10!9!8!!!!!', ''),
(4, 'Camila', 'Rebai', 40855860, 1, '7!7!9!8!7!7!6!7!6!4!7!7!!!!!', '7!6!7!10!5!7!8!8!8!8!7!8!!!!!', '6!8!8!9!9!9!5!8!7!9!8!7!!!!!', ''),
(5, 'Nicolas', 'Bartella', 40900387, 1, '8!7!8!10!8!7!9!9!7!8!8!7!!!!!', '7!8!8!9!7!7!7!8!7!6!6!6!!!!!', '7!5!8!9!8!7!8!7!8!7!8!8!!!!!', ''),
(6, 'NICOLAS', 'TOURIÑO', 35989736, 1, '8!6!9!8!8!5!9!8!5!5!10!9!!!!!', '6!8!9!5!5!8!8!9!9!8!7!6!!!!!', '8!9!6!5!8!6!8!9!5!8!10!8!!!!9!', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` smallint(2) unsigned NOT NULL AUTO_INCREMENT,
  `numero` tinyint(1) unsigned NOT NULL,
  `letra` char(1) COLLATE utf8_bin NOT NULL,
  `materias` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `numero`, `letra`, `materias`) VALUES
(1, 5, 'B', '1,2,3,4,5,6,7,8,9,10,11,17,12,13,14,15,16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE IF NOT EXISTS `materias` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(65) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `abreviatura` varchar(35) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id`, `nombre`, `abreviatura`) VALUES
(1, 'MatemÃ¡tica', 'MatemÃ¡tica'),
(2, 'Literatura', 'Literatura'),
(3, 'InglÃ©s', 'InglÃ©s'),
(4, 'EducaciÃ³n FÃ­sica', 'EducaciÃ³n FÃ­sica'),
(5, 'Historia', 'Historia'),
(6, 'GeografÃ­a', 'GeografÃ­a'),
(7, 'IntroducciÃ³n a la QuÃ­mica', 'IntroducciÃ³n a la QuÃ­mica'),
(8, 'PolÃ­tica y CiudadanÃ­a', 'PolÃ­tica y CiudadanÃ­a'),
(9, 'Elementos de Macro y Micro', 'Elementos de Macro y Micro'),
(10, 'Derecho', 'Derecho'),
(11, 'Sistemas de InformaciÃ³n Contable', 'Sistemas de InformaciÃ³n Contable'),
(12, 'Inasistencias', 'Inasistencias'),
(13, 'Observaciones', 'Observaciones'),
(14, '-', '-'),
(15, 'InformÃ¡tica', 'InformÃ¡tica'),
(16, 'Adeuda Materias', 'Adeuda Materias'),
(17, 'GestiÃ³n Organizacional', 'GestiÃ³n Organizacional');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
