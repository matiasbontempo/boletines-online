$(document).ready(function(){

	console.log("Inicializando.");

	var materias = [];
	var cursos = [];
	var alumnos = [];

	var curso;
	var alumno;

	InitializeSite();
	InitializeRouting();

	OnClicks();

	DetectIntroKey();

});


function InitializeSite() {
	console.log("Cargando Header y NavBar");
	$(".site-head").load("views/header.html");
	$(".nav-wrap").load("views/nav.html");
	GetMaterias();
	GetCursos();
}

function InitializeRouting() {

	Path.map("#/administrar/materias").to(function(){
		$(".content-container").loadAnimate("views/administrar/materias.html", function() {
			$("#sidePanel .panel-body").html("");
			if (typeof materias !== "undefined") {
				materias.forEach(function(materia) {
					$("#sidePanel .panel-body").append('<button type="button" class="btn btn-default btn-sm">'+materia.nombre+'</button>');
				});	
			}
		});
	});

	Path.map("#/administrar/cursos").to(function(){
		$(".content-container").loadAnimate("views/administrar/cursos.html", function(){

            $("#NuevoCursoMaterias").tokenInput(materias, {
				propertyToSearch: "nombre",
				theme: "facebook",
                hintText: "Ingrese Materia",
                noResultsText: "Materia Inexistente",
                searchingText: "Cargando...",
                searchDelay: 0,
                animateDropdown: false,
                preventDuplicates: true
			});

			$("#sidePanel .panel-body").html("");
			
			if (typeof cursos !== "undefined") {
				cursos.forEach(function(_curso) {
					$("#sidePanel .cursos").append('<button type="button" class="btn btn-default btn-sm">'+_curso.numero+'º \''+_curso.letra+'\'</button>');
				});
			}
			
			if (typeof materias !== "undefined") {
				materias.forEach(function(materia) {
					$("#sidePanel .materias").append('<button type="button" class="btn btn-default btn-sm">'+materia.nombre+'</button>');
				});
			}
		});
	});

	Path.map("#/administrar/alumnos").to(function(){
		$(".content-container").loadAnimate("views/administrar/alumnos.html", function() {

            $("#NuevoAlumnoCurso").tokenInput(cursos, {
				propertyToSearch: "display",
				theme: "facebook",
                hintText: "Ingrese Curso",
                noResultsText: "Curso Inexistente",
                searchingText: "Cargando...",
                searchDelay: 0,
                tokenLimit: 1,
                animateDropdown: false,
                preventDuplicates: true
			});

			$("#sidePanel .panel-body").html("");
			
			if (typeof cursos !== "undefined") {
				cursos.forEach(function(_curso) {
					$("#sidePanel .cursos").append('<button type="button" class="btn btn-default btn-sm">'+_curso.numero+'º \''+_curso.letra+'\'</button>');
				});
			}

		});
	});

	Path.map("#/ver/curso/:num/:letra/:id").to(function(){
		var numero = this.params['num'];
		var letra = this.params['letra'];
		var id = this.params['id'];
		$(".content-container").loadAnimate("views/ver/curso.html", function(){
			GetAlumnos(id);
			GetMateriasCurso(id);
			$(".curso-info > h3").text("Curso: "+numero+"º '"+letra+"'");
			$(".imprimir").attr("href", "#/imprimir/boletines/"+id);
		});

	});

	Path.map("#/ver/alumno/:id_alumno").to(function(){
		var id_alumno = this.params['id_alumno'];
		$(".content-container").loadAnimate("views/ver/alumno.html", function() {
			for(i=0; i<alumnos.length; i++) {
				if((alumnos[i].id == id_alumno) && (alumnos[i+1] !== undefined)) {
					$(".next").removeClass("hidden").attr("href", "#/ver/alumno/"+alumnos[i+1].id);
				}
			}
			GetMateriasAlumno(id_alumno);
		});
	});

	Path.map("#/ver/materia/:curso/:materia").to(function(){
		var id_materia = this.params['materia'];
		var id_curso = this.params['curso'];
		$(".content-container").loadAnimate("views/ver/materia.html", function() {
			GetCalificadorMateria(id_materia, id_curso);
		});
	});

	Path.map("#/imprimir/boletines/:num").to(function(){
		var id = this.params['num'];
		$(".content-container").loadAnimate("views/ver/boletines.html", function(){
			GetBoletines(id);
		});
	});


			
	Path.rescue(function(){
		$(".content-container").loadAnimate("views/main.html", function(){
			$.get("views/quotes.txt", function(r){
				var quotes = r.split("??");
				var rand = Math.floor((Math.random() * quotes.length) + 1);
				$(".main > .jumbotron > h1").text(quotes[rand].split("!!")[1]);
				$(".main > .jumbotron > p").text(quotes[rand].split("!!")[0]);
			});
		});
	});
	
	/* Ejemplo
	Path.map("#/posts").to(function(){
	    alert("Posts!");
	}).enter(clearPanel);
	*/
	Path.root("#/main");

	Path.listen();
}

function GetMaterias() {
	console.log("Inicio Cache de Materias");
	php("getMaterias","", function(data) {
		materias = $.parseJSON(data);
		console.log("Finalizacion Cache de Materias");
	});
}

function GetCursos() {
	console.log("Inicio Cache de Cursos");
	php("getCursos","", function(data) {
		cursos = $.parseJSON(data);
		console.log("Finalizacion Cache de Cursos");
		CursosNavbar();
	});
}

function GetAlumnos(id_curso) {
	curso = id_curso;
	console.log("Inicio Cache de Alumnos");
	php("getAlumnos","id="+curso, function(data){
		alumnos = $.parseJSON(data);
		console.log("Finalizacion Cache de Alumnos");
		CargarLista();
	})
}

function GetMateriasCurso(id_curso) {
	php("getMateriasCurso","id_curso="+id_curso, function(data) {
		var _materias = $.parseJSON(data);
		$(".materiasCurso").html('');
		_materias.forEach(function(materia) {
			$(".materiasCurso").append('<a href="#/ver/materia/'+id_curso+'/'+materia.id+'"><label class="label label-primary">'+materia.nombre+'</label></a>');
		});
		ActualizarNotas();
	});
}

function GetMateriasAlumno(id_alumno) {
	php("getMateriasAlumno","id_alumno="+id_alumno, function(data) {
		var _materias = $.parseJSON(data);
		$(".info-alumno > h3").text(_materias.shift());
		$(".info-alumno > p").text(_materias.shift());
		$(".info-alumno").attr("id-alumno", id_alumno);
		$(".notasMaterias").html('');
		_materias.forEach(function(materia) {
			$(".notasMaterias").append('<tr materia="'+materia.id+'"><th>'+materia.nombre+'</th><th><input value="'+materia.trim1+'" trimestre="1" class="selectNote"></th><th><input value="'+materia.trim2+'" trimestre="2" class="selectNote"></th><th><input value="'+materia.trim3+'" trimestre="3" class="selectNote"></th><th></th><th></th></tr>');
		});
		ActualizarNotas();
	});
}

function GetCalificadorMateria(id_materia, id_curso) {
	php("getCalificadorMateria","id_materia="+id_materia+"&id_curso="+id_curso, function(data) {
		var _alumnos = $.parseJSON(data);
		$(".info-materia > p").text(_alumnos.shift());
		$(".info-materia > h3").text(_alumnos.shift());
		var index = _alumnos.shift();
		$(".notasMaterias").html('');
		_alumnos.forEach(function(alumno) {
			$(".notasMaterias").append('<tr alumno="'+alumno.id+'" curso="'+index+'" index-materia="'+index+'"><th>'+alumno.apellido+", "+alumno.nombre+'</th><th><input value="'+alumno.trim1+'" trimestre="1" class="selectNote"></th><th><input value="'+alumno.trim2+'" trimestre="2" class="selectNote"></th><th><input value="'+alumno.trim3+'" trimestre="3" class="selectNote"></th><th></th><th></th></tr>');
		});
		ActualizarNotas();
	});
}

function GetBoletines(id_curso) {
	php("getBoletines","id_curso="+id_curso, function(data) {
		var data = $.parseJSON(data);
		var curso = data.curso;
		$(".boletines").html('');
		data.alumnos.forEach(function(_alumno) {
			var trim1 = _alumno.trim1.split("!");
			var trim2 = _alumno.trim2.split("!");
			var trim3 = _alumno.trim3.split("!");
			$(".boletines").append('<div class="col-xs-6"><div class="panel panel-lined boletin"><div class="panel-heading center">CICLO LECTIVO 2015 - '+curso.id+'</div><div class="panel-body"><div class="left"><img src="images/logo.png"/></div><div class="left"><b>INSTITUTO PRECIOSISIMA SANGRE</b><br /><small>ESCUELA RECONOCIDA<br />DIPREGEP 7213<br />ECHEVERRIA 6245 - VILLA BOSCH<br />TRES DE FEBRERO, BS. AS.</small></div></div><table class="table"><thead><tr><th><b>'+_alumno.apellido+', '+_alumno.nombre+'</b><br />'+_alumno.dni+'</th><th>1er Trim</th><th>2do Trim</th><th>3er Trim</th><th>Prom</th><th>Final</th></tr></thead><tbody class="notasMaterias"></tbody></table><div class="row firmas"><div class="col-xs-4">Firma Alumno</div><div class="col-xs-4">Firma Padre/Madre/Tutor</div><div class="col-xs-4">Firma Directora</div></div></div></div>');
			curso.materias.forEach(function(materia, index) {
				var promedio = (parseInt(trim1[index]) + parseInt(trim2[index]) + parseInt(trim3[index])) / 3;
				var final = promedio < 7 ? "" : Truncar(promedio);
				
				if (isNaN(promedio)) {
					promedio = "";
					final = "";
				}

				t1 = trim1[index]?trim1[index]:"";
				t2 = trim2[index]?trim2[index]:"";
				t3 = trim3[index]?trim3[index]:"";
				
				$(".notasMaterias:last").append('<tr><th>'+materia+'</th><th>'+t1+'</th><th>'+t2+'</th><th>'+t3+'</th><th>'+Truncar(promedio)+'</th><th>'+final+'</th></tr>');
			});
		});
	});
}



function OnClicks() {
}

function DetectIntroKey() {
	$(document).keyup(function(e) {
	    if (e.keyCode == 13) {
	    	event.preventDefault();

	    	if ($(document.activeElement).attr("trimestre")) {

	    		selected = document.activeElement;

	    		if ($(selected).parent().parent().attr("materia")) {

	    			var notas = [];
		    		var trimestre = $(document.activeElement).attr("trimestre");
		    		var alumno = $(".info-alumno").attr("id-alumno");
		    		//$('input[trimestre="1"]').each(function() {
		    		$('input[trimestre="'+trimestre+'"]').each(function() {
		    			notas.push($(this).val());
		    		});

		    		php("actualizarNotasAlumno", "id_alumno="+alumno+"&trimestre="+trimestre+"&notas="+notas.join("!"), function(data) {
					    	var tmp = parseInt(trimestre) + 1;
					    	$(document.activeElement).addClass("notaOk");
					    	$(document.activeElement).parent().parent().next().children("th:nth-child(" + tmp + ")").children().focus();
					    	ActualizarNotas();
		    		});

	    		} else {

	    			var alumno = $(selected).parent().parent().attr("alumno");
	    			var trimestre = $(selected).attr("trimestre");
	    			var index_materia = parseInt($(selected).parent().parent().attr("index-materia")) + 1;
	    			var nota = $(selected).val();

		    		php("actualizarNotasMateria", "id_alumno="+alumno+"&trimestre="+trimestre+"&index_materia="+index_materia+"&nota="+nota, function(data) {
		    				console.info(data);
					    	var tmp = parseInt(trimestre) + 1;
					    	$(document.activeElement).addClass("notaOk");
					    	$(document.activeElement).parent().parent().next().children("th:nth-child(" + tmp + ")").children().focus();
					    	ActualizarNotas();
		    		});
	    		}

	    	}
	    }
	});
}



/* ------- OnClick Functions --------- */

function NuevaMateria() {

	console.log("Inicio de Creacion de Materia");
	
	$(this).attr("disabled");

	var nombre = $("#NuevaMateriaNombre").val();
	var abrv = $("#NuevaMateriaAbreviatura").val();

	php("nuevaMateria","nombre="+nombre+"&abrv="+abrv, function(data) {
		toast(data, "success");
		$("#sidePanel .panel-body").append('<button type="button" class="btn btn-default btn-sm">'+nombre+'</button>');
		GetMaterias();
	});
}

function NuevoCurso() {

	console.log("Inicio de Creacion de Curso");
	
	$(this).attr("disabled");

	var numero = $('#NuevoCursoNumero').val();
	var letra = $('#NuevoCursoLetra').val();
	var materias = $('#NuevoCursoMaterias').val();

	php("nuevoCurso","numero="+numero+"&letra="+letra+"&materias="+materias, function(data) {
		toast(data, "success");
		$("#sidePanel .cursos").append('<button type="button" class="btn btn-default btn-sm">'+numero+'º \''+letra+'\'</button>');
		GetCursos();
	});
}

function NuevoAlumno() {

	console.log("Inicio de Creacion de Alumno");
	
	$(this).attr("disabled");

	var nombre = $('#NuevoAlumnoNombre').val();
	var apellido = $('#NuevoAlumnoApellido').val();
	var dni = $('#NuevoAlumnoDni').val();
	var curso = $('#NuevoAlumnoCurso').val();

	php("nuevoAlumno","nombre="+nombre+"&apellido="+apellido+"&dni="+dni+"&curso="+curso, function(data) {
		toast(data, "success");
		//GetAlumnos();
	});
}


function CursosNavbar() {

	console.log("Cargando cursos en NavBar");

	if (typeof cursos !== "undefined") {
		cursos.forEach(function(_curso) {
			$(".nav-list.cursos").append('<li><a href="#/ver/curso/'+_curso.numero+'/'+_curso.letra+'/'+_curso.id+'"><i class="fa fa-paper-plane icon"></i><span class="text">'+_curso.numero+'º '+_curso.letra+'</span></a></li>');
		});	
	}
}

function CargarLista() {
	console.log("Cargando Lista de Alumnos");
	//$(".curso-info > h3").text("Curso: "+numero+"º '"+letra+"'");
	$(".curso-info > .small").html(alumnos.length + " Alumnos");
	if (typeof alumnos !== "undefined") {
		var i = 1;
		$(".table > tbody").html("");
		alumnos.forEach(function(_alumno) {
			$(".table > tbody").append('<tr><td>'+i+'</td><td>'+_alumno.apellido+', '+_alumno.nombre+'</td><td><a class="label label-info" href="#/ver/alumno/'+_alumno.id+'"><i class="icon fa fa-book"></i></a><a class="label label-danger"><i class="icon fa fa-remove"></i></a></td></tr>');
			i++;
		});	
	}
}

function ActualizarNotas() {
	$('tr[materia],tr[alumno]').each(function() {
		var materia = $(this).attr("materia");
		var promedio;
		var trim = [0,
			parseFloat($(this).children("th:nth-child(2)").children("input").val()),
			parseFloat($(this).children("th:nth-child(3)").children("input").val()),
			parseFloat($(this).children("th:nth-child(4)").children("input").val()),
		];

		if (trim[1] > 0 && trim[2] > 0 && trim[3] > 0) promedio = ((trim[1] + trim[2] + trim[3]) / 3);
		else promedio = 0;

		if (promedio) $(this).children("th:nth-child(5)").html(Truncar(promedio));
		else $(this).children("th:nth-child(5)").html("");

		if ((promedio < 7) || trim[1] < 4 || trim[2] < 4 || trim[3] < 4) promedio = "";
		else promedio = Truncar(promedio);

		$(this).children("th:nth-child(6)").html(promedio);
	});
}















function php(accion, data, success) {

	$.ajax({
		type: "POST",
		url: "./scripts/api.php?do="+accion,
		data: data,
		dataType: "json",
		timeout: 6000,
		error: function(objeto, quepaso, otroobj){
			toast("<strong>Error: </strong> No se pudo conectar con el servidor.<br />"+quepaso+" "+otroobj, "danger");
			//console.log("%o", objeto);
			console.warn(objeto.responseText);
		},
		success: function(data) {
			if (data.extra) console.alert(data.extra);
			if (data.status == "OK") {
				success(data.data);
			} else {
				toast("<strong>Ha ocurrido un error. </strong>Intente nuevamente.<br />"+data.data, "danger");
				console.warn(data);
			}
		}
	});
}

function toast(msg, type) {
	type = typeof type === "undefined" ? "default" : type;
	$("body").append('<div class="toast toast-bottomRight">\
						<div class="toast-fade alert alert alert-'+type+' alert-dismissable" role="alert" type="danger">\
    						<button type="button" class="close">\
        						<span aria-hidden="true">×</span>\
        						<span class="sr-only">Close</span>\
    						</button>\
    						<div>\
								<div class="ng-binding ng-scope">\
									'+msg+'\
								</div>\
							</div>\
						</div>\
					</div>');

	$('.toast').hide().fadeIn(400).delay(5000).fadeOut(400, function(){$(this).remove()});

	switch(type) {
		case "default":
		case "info":
			console.log(msg);
			break;
		case "alert":
			console.warn(msg);
			break;
		case "danger":
			console.error(msg);
			break;
	}
}

function Truncar(num) {
	if(typeof(num) === "number") return parseInt(num * 100)/100;
	else return "";
}

(function( $ ) {
 
    $.fn.loadAnimate = function(url, callback) {

    	var localThis = this;

		localThis.removeClass("ng-enter");

		setTimeout(function(){
			localThis.load(url, function() {
				localThis.addClass("ng-enter");
				if (callback) callback();
			});
		}, 150);
    };
 
}( jQuery ));

