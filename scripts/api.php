<?php

require_once("./include/db.php");
require_once("./include/functions.php");

$json = array(
	"status" => "ERROR",
	"data" => "",
	"extra" => "",
	"accion" => $_GET['do']
);

if (isset($_GET['do'])) {

	$do = escape_data($_GET['do'], "varchar");

	switch($do) {

		case "logIn":
			LogIn();
		break;
		case "logOut":
			LogOut();
		break;

		case "nuevaMateria":
			NuevaMateria();
		break;
		case "nuevoCurso":
			NuevoCurso();
		break;
		case "nuevoAlumno":
			NuevoAlumno();
		break;

		case "getMaterias":
			GetMaterias();
		break;
		case "getCursos":
			GetCursos();
		break;
		case "getAlumnos":
			GetAlumnos();
		break;

		case "getMateriasCurso":
			GetMateriasCurso();
		break;

		case "getMateriasAlumno":
			GetMateriasAlumno();
		break;

		case "getCalificadorMateria":
			GetCalificadorMateria();
		break;

		case "actualizarNotasAlumno":
			ActualizarNotasAlumno();
		break;

		case "actualizarNotasMateria":
			ActualizarNotasMateria();
		break;

		case "getBoletines":
			GetBoletines();
		break;

		default:
			$json['data'] = "Acción inexiste.";
		break;

	}

	echo json_encode($json);
	return;
}

/*  -----  FUNCIONES  -----  */

function NuevaMateria() {
	
	global $json;

	$nombre = escape_data($_POST['nombre']);
	$abrv = escape_data($_POST['abrv']);

	if (!$nombre) {
		$json['data'] = "Campos invalidos.";
		return;
	}

	if (!$abrv) {
		$abrv = $nombre;
	}

	$sql = "SELECT id FROM materias WHERE nombre = '$nombre'";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs)) {
		$json['data'] = $nombre+" ya existe.";
		return;
	}

	$sql = "INSERT INTO materias (nombre, abreviatura) VALUES ('".$nombre."', '".$abrv."')";
	$rs = mysql_query($sql);

	if (mysql_affected_rows() == 1) {
		$json['data'] = "Materia creada correctamente.";
		$json['status'] = "OK";
	}

}

function NuevoCurso() {
	
	global $json;

	$numero = escape_data($_POST['numero'], "int");
	$letra = escape_data($_POST['letra'], "varchar");
	$materias = escape_data($_POST['materias']);

	if (!$numero || !$letra || !$materias) {
		$json['data'] = "Campos invalidos.";
		return;
	}

	$sql = "SELECT id FROM cursos WHERE numero = '$numero' AND letra = '$letra'";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs)) {
		$json['data'] = $numero+$letra+" ya existe.";
		return;
	}

	$sql = "INSERT INTO cursos (numero, letra, materias) VALUES ('".$numero."', '".strtoupper($letra)."', '".$materias."')";
	$rs = mysql_query($sql);

	if (mysql_affected_rows() == 1) {
		$json['data'] = $numero + "º '" + $letra + "'' creado correctamente.";
		$json['status'] = "OK";
	}

}

function NuevoAlumno() {
	
	global $json;

	$nombre = escape_data($_POST['nombre'], "varchar");
	$apellido = escape_data($_POST['apellido'], "varchar");
	$dni = escape_data($_POST['dni'], "int");
	$curso = escape_data($_POST['curso'], "int");


	if (!$nombre || !$apellido || !$dni || !$curso) {
		$json['data'] = "Campos invalidos.";
		return;
	}

	$sql = "SELECT id FROM alumnos WHERE dni = '$dni'";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs)) {
		$json['data'] = "El alumno ya existe ya existe.";
		return;
	}

	$sql = "INSERT INTO alumnos (nombre, apellido, dni, id_curso) VALUES ('".$nombre."', '".$apellido."', '".$dni."', '".$curso."')";
	$rs = mysql_query($sql);


	$json['data'] = $sql;

	if (mysql_affected_rows() == 1) {
		$json['data'] = "Alumno creado correctamente.";
		$json['status'] = "OK";
	}

}




function GetMaterias() {
	
	global $json;
	$tmp_array = array();

	$sql = "SELECT id, nombre FROM materias ORDER BY nombre ASC";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs) == 0) {
		$json['data'] = "No hay materias.";
		return;
	}

	while($materia = mysql_fetch_array($rs)) {
		array_push($tmp_array, [
			"id" => $materia["id"],
			"nombre" => $materia["nombre"]
		]);
	}

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";

}

function GetCursos() {
	
	global $json;
	$tmp_array = array();

	$sql = "SELECT id, numero, letra FROM cursos ORDER BY numero ASC, letra ASC";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs) == 0) {
		$json['data'] = "No hay cursos.";
		return;
	}

	while($curso = mysql_fetch_array($rs)) {
		array_push($tmp_array, [
			"id" => $curso["id"],
			"display" => (string)$curso["numero"].$curso["letra"],
			"numero" => $curso["numero"],
			"letra" => $curso["letra"]
		]);
	}

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";

}

function GetAlumnos() {
	
	global $json;
	$tmp_array = array();

	$id = escape_data($_POST['id'], "int");

	if (!$id) {
		$json['data'] = "Valor invalido.";
		return;
	}

	$sql = "SELECT id, nombre, apellido FROM alumnos WHERE id_curso = '".$id."' ORDER BY apellido ASC, nombre ASC";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs) == 0) {
		$json['data'] = "No hay alumnos.";
		return;
	}

	while($alumno = mysql_fetch_array($rs)) {
		array_push($tmp_array, [
			"id" => $alumno["id"],
			"nombre" => $alumno["nombre"],
			"apellido" => $alumno["apellido"]
		]);
	}

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";

}

function GetMateriasCurso() {
	
	global $json;
	$tmp_array = array();

	$id_curso = escape_data($_POST['id_curso'], "int");

	$sql = "SELECT materias FROM cursos WHERE id = ".$id_curso;
	$rs = mysql_query($sql);

	$materias = mysql_fetch_array($rs)['materias'];

	$sql = "SELECT id, nombre FROM materias WHERE id IN(".$materias.") ORDER BY FIELD(id,".$materias.")";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs) == 0) {
		$json['data'] = "No hay materias.";
		return;
	}

	$i = 0;

	while($materia = mysql_fetch_array($rs)) {
		array_push($tmp_array, [
			"id" => $materia["id"],
			"nombre" => $materia["nombre"]
		]);
		$i++;
	}

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";
}

function GetMateriasAlumno() {
	
	global $json;
	$tmp_array = array();

	$id_alumno = escape_data($_POST['id_alumno'], "int");

	$sql = "SELECT nombre, apellido, materias, numero, letra FROM cursos JOIN alumnos ON alumnos.id_curso = cursos.id WHERE alumnos.id = ".$id_alumno;
	$rs = mysql_query($sql);

	$data = mysql_fetch_array($rs);

	if ($data['numero'] == "1" || $data['numero'] == "3") $curso = $data['numero']."ro ".$data['letra'];
	else if ($data['numero'] == "2") $curso = $data['numero']."do ".$data['letra'];
	else if ($data['numero'] == "7") $curso = $data['numero']."mo ".$data['letra'];
	else if ($data['numero'] == "8") $curso = $data['numero']."vo ".$data['letra'];
	else if ($data['numero'] == "9") $curso = $data['numero']."no ".$data['letra'];
	else $curso = $data['numero']."to ".$data['letra'];

	array_push($tmp_array, $data['apellido'].", ".$data['nombre']);
	array_push($tmp_array, $curso);

	$materias = $data['materias'];

	$sql = "SELECT id, nombre FROM materias WHERE id IN(".$materias.") ORDER BY FIELD(id,".$materias.")";
	$rs = mysql_query($sql);

	if (mysql_num_rows($rs) == 0) {
		$json['data'] = "No hay materias.";
		return;
	}

	$sql = "SELECT trim1, trim2, trim3 FROM alumnos WHERE id = ".$id_alumno;
	$rs2 = mysql_query($sql);

	$trimestres = mysql_fetch_array($rs2);
	
	$trim1 = explode("!", $trimestres['trim1']); 
	$trim2 = explode("!", $trimestres['trim2']); 
	$trim3 = explode("!", $trimestres['trim3']);

	$i = 0;

	while($materia = mysql_fetch_array($rs)) {
		array_push($tmp_array, [
			"id" => $materia["id"],
			"nombre" => $materia["nombre"],
			"trim1" => isset($trim1[$i]) ? $trim1[$i] : "",
			"trim2" => isset($trim2[$i]) ? $trim2[$i] : "",
			"trim3" => isset($trim3[$i]) ? $trim3[$i] : ""
		]);
		$i++;
	}

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";
}

function GetCalificadorMateria() {

	global $json;
	$tmp_array = array();

	$id_curso = escape_data($_POST['id_curso'], "int");
	$id_materia = escape_data($_POST['id_materia'], "int");
	$index_materia = 1;
	$nombre_materia = "";

	$sql = "SELECT numero, letra, materias FROM cursos WHERE id = ".$id_curso;
	$rs = mysql_query($sql);

	$data = mysql_fetch_array($rs);

	if ($data['numero'] == "1" || $data['numero'] == "3") $curso = $data['numero']."ro ".$data['letra'];
	else if ($data['numero'] == "2") $curso = $data['numero']."do ".$data['letra'];
	else if ($data['numero'] == "7") $curso = $data['numero']."mo ".$data['letra'];
	else if ($data['numero'] == "8") $curso = $data['numero']."vo ".$data['letra'];
	else if ($data['numero'] == "9") $curso = $data['numero']."no ".$data['letra'];
	else $curso = $data['numero']."to ".$data['letra'];

	$materias = explode(",", $data['materias']);
	$index_materia = array_search($id_materia, $materias);

	$sql = "SELECT nombre FROM materias WHERE id =".$id_materia;
	$rs = mysql_query($sql);
	$nombre_materia = mysql_fetch_array($rs)['nombre'];

	array_push($tmp_array, $curso);
	array_push($tmp_array, $nombre_materia);
	array_push($tmp_array, $index_materia);

	$sql = "SELECT id, nombre, apellido, trim1, trim2, trim3 FROM alumnos WHERE id_curso = ".$id_curso." ORDER BY apellido ASC, nombre ASC";
	$rs = mysql_query($sql);

	while ($alumno = mysql_fetch_array($rs)) {
		$trim1 = isset($alumno['trim1'])?$alumno['trim1']:"";
		$trim2 = isset($alumno['trim2'])?$alumno['trim2']:"";
		$trim3 = isset($alumno['trim3'])?$alumno['trim3']:"";

		$trim1 = isset(explode("!", $trim1)[$index_materia])?explode("!", $trim1)[$index_materia]:"";
		$trim2 = isset(explode("!", $trim2)[$index_materia])?explode("!", $trim2)[$index_materia]:"";
		$trim3 = isset(explode("!", $trim3)[$index_materia])?explode("!", $trim3)[$index_materia]:"";
		
		array_push($tmp_array, [
			"id" => $alumno['id'],
			"nombre" => $alumno['nombre'],
			"apellido" => $alumno['apellido'],
			"trim1" => $trim1,
			"trim2" => $trim2,
			"trim3" => $trim3
		]);
	}

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";

}

function ActualizarNotasAlumno() {
	
	global $json;

	$id_alumno = escape_data($_POST['id_alumno'], "int");
	$trimestre = escape_data($_POST['trimestre'], "int");
	$notas = escape_data($_POST['notas'], "custom", '%^[0-9!\.\,]+$%');

	if (!$id_alumno || !$trimestre || !$notas) {
		$json['data'] = "Valores invalidos.";
		return;
	}

	$sql = "UPDATE alumnos SET trim".$trimestre."='".$notas."' WHERE id='".$id_alumno."'";
	$rs = mysql_query($sql);

	if (mysql_affected_rows() <= 1) {
		$json['status'] = "OK";
	}
}

function ActualizarNotasMateria() {
	
	global $json;

	$id_alumno = escape_data($_POST['id_alumno'], "int");
	$trimestre = escape_data($_POST['trimestre'], "int");
	$index_materia = escape_data($_POST['index_materia'], "int");
	$nota = escape_data($_POST['nota'], "custom", '%^[0-9!\.\,]+$%');

	if (!$id_alumno || !$trimestre || !$index_materia || !$nota) {
		$json['data'] = "Valores invalidos.";
		return;
	}

	$sql = "SELECT trim".$trimestre." AS notas FROM alumnos WHERE id = ".$id_alumno;
	$rs = mysql_query($sql);

	$notas = mysql_fetch_array($rs)['notas'];

	if ($notas) {
		$notas = explode("!", $notas);
		$notas[$index_materia-1] = $nota;
		$notas = implode('!',$notas);
	} else {
		$json['data'] = "Crear Notas";
	}

	$sql = "UPDATE alumnos SET trim".$trimestre."='".$notas."' WHERE id='".$id_alumno."'";
	$rs = mysql_query($sql);

	if (mysql_affected_rows() <= 1) {
		$json['status'] = "OK";
	}
}

function GetBoletines() {
	
	global $json;
	$tmp_array = array();
	$curso_array = array();
	$materias_array = array();
	$alumnos_array = array();

	$id_curso = escape_data($_POST['id_curso'], "int");
	$id_alumno = isset($_POST['id_alumno']) ? escape_data($_POST['id_alumno'], "int") : false;

	if (!$id_curso) {
		$json['data'] = "Valores invalidos.";
		return;
	}

	$sql = "SELECT numero, letra, materias FROM cursos WHERE id=".$id_curso;
	$rs = mysql_query($sql);

	$curso = mysql_fetch_array($rs);

	$sql = "SELECT id, nombre FROM materias WHERE id IN(".$curso['materias'].") ORDER BY FIELD(id,".$curso['materias'].")";
	$rs = mysql_query($sql);

	while($materia = mysql_fetch_array($rs)) {
		array_push($materias_array, $materia["nombre"]);
	}

	$curso_array = [
		"id" => $curso["numero"]."º '".$curso["letra"]."'",
		"materias" => $materias_array
	];

	// SELECCIONAR ALUMNOS DE UN CURSO
	$sql = "SELECT nombre, apellido, dni, trim1, trim2, trim3 FROM alumnos WHERE id_curso=".$id_curso." ORDER BY apellido ASC";
	if ($id_alumno) $sql .= "AND id=".$id_alumno;
	$rs = mysql_query($sql);

	while($alumno = mysql_fetch_array($rs)) {
		array_push($alumnos_array, [
			"nombre" => $alumno["nombre"],
			"apellido" => $alumno["apellido"],
			"dni" => $alumno["dni"],
			"trim1" => $alumno["trim1"],
			"trim2" => $alumno["trim2"],
			"trim3" => $alumno["trim3"]
		]);
	}

	$tmp_array = [
		"curso" => $curso_array,
		"alumnos" => $alumnos_array
	];

	$json['data'] = json_encode($tmp_array);
	$json['status'] = "OK";

	// SELECCIONAR ID MATERIAS DE UN CURSO Y LUEGO LOS NOMBRES DE LAS MATERIAS

}

?>