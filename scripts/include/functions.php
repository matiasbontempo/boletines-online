<?php

function escape_data($data, $type = "text", $customEval = "0") {

	switch ($type) {
		case "int":
			$eval = '%^[0-9]+$%';
		break;
		case "varchar":
			$eval = '%^[A-Za-zÑñáéíóúÁÉÍÓÚ\.\'\-\s]+$%';
		break;
		case "text":
			$eval = '%^[A-Za-z0-9^${}()+ÑñáéíóúÁÉÍÓÚ?!\.\,\'\-\\\[\]\s]+$%';
		break;
		case "custom":
			$eval = $customEval;
		break;
	}
	
	if(preg_match($eval, stripslashes(trim($data)))) {
		if (function_exists('mysql_real_escape_string') && isset($con)) {
			global $con;
			$data = mysql_real_escape_string(trim($data), $con);
			$data = strip_tags($data);
		} else {
			$data = mysql_escape_string(trim($data));
			$data = strip_tags($data);
		}
	} else {
		$data = FALSE;
	}
	return $data;
}

?>